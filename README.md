# Cloud-Init and Zero-Trust

Why should I care about putting any valid access or SSH-Keys in User-Data?

That is the wrong question. 

I should not trust anyone, not even the Cloud-Provider if it comes to "Access" Keys.

That is why I want to demonstrate a way how to install SSH-Keys which are only valid for a short period of time

The Scripts:

- **doall**: It is doing all the skripts in one row.
- **create_keys**: It is creating SSH-Keys for gitlab access and registers the public-Key in Gitlab using an accesstoken
- **create_user_data**: It is creating the user-data.yml including the private key just created for installation. 
- **create_vm_`provider`**: It launches the vm using cloud-init user-data.yml on a `provider`.
- **delete_keys**: It deletes the keys created above from gitlab again. No trust anymore. Nobody can use the private key anymore to access gitlab.

## Requirements

GPG key for accesstoken.gpg which is used to access API of gitlab.com. You may change this token to use yours. This can be done by changing the code in doall or just by replacing accesstoken.gpg

## multipass

### Requirements
[multipass](https://multipass.run/) is installed

### Scripts
- **create_vm_multipass**: create the VM using multipass

### Requirements
## AWS

### Requirements

[AWS CLI](https://aws.amazon.com/cli/) is installed
 and AWS CLI is configured with accestoken

### Scripts
- **create_vm_aws**: create the VM in aws Cloud
- **aws_create_security_groups**: Creates the security group
- **aws_terminate**: Terminates the instance taged with securevm
- **aws_terminate_all**: Terminates all VMs in AWS Cloud in account

